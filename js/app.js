function generateCinema(row, col) {

    var htmlString = [];

    for (var i = 0; i < row; i++) {
        // example of an array push


        for (j = 0; j < col; j++) {

            htmlString.push(createSeat(i, j));
        }

        htmlString.push(createNewRow());


    } //for

    return htmlString.join(" ");

}

function createSeat(row, col) {

    var attributes = "id=seat_" + row + "_" + col;
    attributes += " data-row='" + row + "'";
    attributes += " data-col='" + col + "'";;

    var seat = "<div " + attributes + " class='seat'></div>"

    return seat;

}

function createNewRow() {

    var seat = "<div class='end'></div>"

    return seat;

}


function mapSeating(seating) {


    for (var i = 0; i < seating.selected.length; i++) {

        var seatId = '#seat_' + seating.selected[i].r + '_' + seating.selected[i].c;
        $(seatId).addClass("selected");

    }

    for (var i = 0; i < seating.reserved.length; i++) {

        var seatId = '#seat_' + seating.reserved[i].r + '_' + seating.reserved[i].c;
        $(seatId).addClass("reserved");

    }

    for (var i = 0; i < seating.aisle.length; i++) {

        var seatId = '#seat_' + seating.aisle[i].r + '_' + seating.aisle[i].c;
        $(seatId).addClass("aisle");

    }


}

function retrieveSeatingAllocations() {

    var seating = {
        "selected": [{ r: 1, c: 2 }, { r: 1, c: 3 }, { r: 1, c: 4 }],
        "reserved": [{ r: 2, c: 2 }, { r: 3, c: 3 }, { r: 4, c: 4 }],
        "aisle": [{ r: 0, c: 7 }, { r: 0, c: 8 }, { r: 1, c: 7 }, { r: 1, c: 8 },{ r: 2, c: 7 }, { r: 2, c: 8 }]
    }

return seating;

}


function setupSeating(seatsBooked) {


         $('#seating').on('click', '.seat', function () {

                var currentId = $(this).attr('id');
                console.log("you clicked on ", currentId);


                if (!$(this).hasClass("reserved") && !$(this).hasClass("aisle")) {

                    if (!$(this).hasClass("selected")) {

                        var seatsSelected = $(".selected").length
                        if (seatsSelected >= seatsBooked) return;

                        $(this).addClass("selected");
                    } else {
                        $(this).removeClass("selected");
                    }
                }





                console.log("Total selected is ", $(".selected").length)

            });
}

